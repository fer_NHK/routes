Highcharts.chart('graficaEstadistica', {
    chart: {
        type: 'solidgauge',
        marginTop: 0,
        backgroundColor: 'none'
    },
    credits: {
        enabled: false
    },
    title: {
        text: '',
        style: {
            fontSize: '24px'
        }
    },
    tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '10px'
        },
        pointFormat: '{series.name}<br><span style="font-size:12px; color:#000000; font-weight: bold">{point.y}%</span>'
    },
    pane: {
        startAngle: -90,
        endAngle: 90,
        background: [{ // Track for Move
            outerRadius: '112%',
            innerRadius: '88%',
            backgroundColor: 'none',
            borderWidth: 0
        }, { // Track for Exercise
            outerRadius: '87%',
            innerRadius: '63%',
            backgroundColor: 'none',
            borderWidth: 0
        }, { // Track for Stand
            outerRadius: '62%',
            innerRadius: '38%',
            backgroundColor: 'none',
            borderWidth: 0
        }]
    },
    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: [],
        labels: {
            y: 16
        }
    },
    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
                //y: 5,
                //borderWidth: 0,
                //useHTML: true
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: false
        },
    },
    series: [{
        name: 'Esperadas',
        colorByPoint: true,
        data: [{
            color: '#d2067f',
            radius: '100%',
            innerRadius: '88%',
            y: 100
        }],
        dataLabels: {
            //enabled: true,
            format: '<div style="text-align:center;"><span style="color:#F4CEE8; font-weight: bold">{series.name}</span><br><span style="font-size:12px; color: {point.color}; font-weight: bold">{point.y}%</span></div>'
        }
    }, {
        name: 'Capturadas',
        colorByPoint: true,
        data: [{
            color: '#e73a8c',
            radius: '87%',
            innerRadius: '75%',
            y: 70
        }],
        dataLabels: {
            format: '<div style="text-align:center;><span style="color:#F4CEE8; font-weight: bold">{series.name}</span><br><span style="font-size:12px; color: {point.color}; font-weight: bold">{point.y}%</span></div>'
        }
    }, {
        name: 'Contabilizadas',
        colorByPoint: true,
        data: [{
            color: '#804682',
            radius: '74%',
            innerRadius: '62%',
            y: 20
        }],
        dataLabels: {
            format: '<div style="text-align:center;"><span style="color:#F4CEE8; font-weight: bold">{series.name}</span><br><span style="font-size:12px; color: {point.color}; font-weight: bold">{point.y}%</span></div>'
        }
    }]
});
Highcharts.chart('grafica', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Elecciones Estatales de Quintana Roo'
    },
    subtitle: {
        text: 'Programa de Resultados Electorales Preliminares'
    },
    xAxis: {
        categories: ['PAN', 'PRI', 'PRD', 'PVEM', 'PT', 'MC', 'NA', 'MORENA', 'ES', 'CI1', 'Candidaturas no registradas', 'Votos nulos'],
        labels: {
            useHTML: true,
            formatter: function() {
                if (this.value == "MC") return '<img src="img/mc.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "PAN") return '<img src="img/pan.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "PRI") return '<img src="img/pri.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "PRD") return '<img src="img/prd.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "PVEM") return '<img src="img/pvem.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "PT") return '<img src="img/pt.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "MC") return '<img src="img/mc.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "NA") return '<img src="img/na.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "MORENA") return '<img src="img/morena.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "ES") return '<img src="img/es.png" style="width: 50px; vertical-align: middle" />';
                else if (this.value == "CI1") return '<img src="img/ci_01.png" style="width: 50px; vertical-align: middle" />';
                else return this.value;
            }
        },
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Votos'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table >',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Votos',
        data: [49, 71, 106, 129, 144, 176, 135, 148, 216, 194, 95, 300],
        color: 'rgb(99, 42, 91)'
    }]
});