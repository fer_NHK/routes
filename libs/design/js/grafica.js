Highcharts.chart('graficaEstadistica', {
    chart: {
        type: 'solidgauge',
        marginTop: 0,
        backgroundColor: 'none'
    },
    credits: {
        enabled: false
    },
    title: {
        text: '',
        style: {
            fontSize: '24px'
        }
    },
    tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '17px'
        },
        pointFormat: '{series.name}<br><span  style="font-size:15px; color:#000000; font-weight: bold">{point.y:.4f}%</span>'
    },
    pane: {
        startAngle: -90,
        endAngle: 90,
        background: [{ // Track for Move
            outerRadius: '112%',
            innerRadius: '88%',
            backgroundColor: 'none',
            borderWidth: 0
        }, { // Track for Exercise
            outerRadius: '87%',
            innerRadius: '63%',
            backgroundColor: 'none',
            borderWidth: 0
        }, { // Track for Stand
            outerRadius: '62%',
            innerRadius: '38%',
            backgroundColor: 'none',
            borderWidth: 0
        }]
    },
    yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: [],
        labels: {
            y: 16
        }
    },
    plotOptions: {
        solidgauge: {
            dataLabels: {
                enabled: false
                //y: 5,
                //borderWidth: 0,
                //useHTML: true
            },
            linecap: 'round',
            stickyTracking: false,
            rounded: false
        },
    },
    series: [{
        name: 'Esperadas',
        colorByPoint: true,
        data: [{
            color: '#d2067f',
            radius: '100%',
            innerRadius: '88%',
            y: 100
        }],
        dataLabels: {
            //enabled: true,
            format: '<div ><span style="color:#F4CEE8; font-weight: bold">{series.name}</span><br><span  style="font-size:12px; color: {point.color}; font-weight: bold">{point.y}%</span></div>'
        }
    }, {
        name: 'Capturadas',
        colorByPoint: true,
        data: [{
            color: '#e73a8c',
            radius: '87%',
            innerRadius: '75%',
            y: 70
        }],
        dataLabels: {
            format: '<div ><span style="color:#F4CEE8; font-weight: bold">{series.name}</span><br><span style="font-size:12px; color: {point.color}; font-weight: bold">{point.y}%</span></div>'
        }
    }, {
        name: 'Contabilizadas',
        colorByPoint: true,
        data: [{
            color: '#804682',
            radius: '74%',
            innerRadius: '62%',
            y: 20
        }],
        dataLabels: {
            format: '<div ><span style="color:#F4CEE8; font-weight: bold">{series.name}</span><br><span style="font-size:12px; color: {point.color}; font-weight: bold">{point.y}%</span></div>'
        }
    }]
});
Highcharts.chart('grafica', {
    chart: {
        type: 'column'
    },
    title: {
        text: '' //Elecciones Estatales de Quintana Roo
    },
    subtitle: {
        text: '' //Programa de Resultados Electorales Preliminares
    },
    xAxis: {
        categories: ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'CI', 'Candidaturas no registradas', 'Votos nulos', 'Total'],
        labels: {
            useHTML: true,
            style: {
                width: '10%'
            },
            formatter: function() {
                if (this.value == "P1") return '<img data-original-title="Partido 1" data-placement="bottom" data-toggle="tooltip"  src="img/rep_prep/p1.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P2") return '<img data-original-title="Partido 2" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p2.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P3") return '<img data-original-title="Partido 3" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p3.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P4") return '<img data-original-title="Partido 4" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p4.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P5") return '<img data-original-title="Partido 5" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p5.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P6") return '<img data-original-title="Partido 6" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p6.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P7") return '<img data-original-title="Partido 7" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p7.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P8") return '<img data-original-title="Partido 8" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p8.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "P9") return '<img data-original-title="Partido 9" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/p9.png" style="width: 40px; vertical-align: middle" />';
                else if (this.value == "CI") return '<img data-original-title="Candidatura Independiente" data-placement="bottom" data-toggle="tooltip" src="img/rep_prep/ci.png" style="width: 40px; vertical-align: middle" />';
                else return '<p class="text-center" data-original-title="' + this.value + '" data-placement="bottom" data-toggle="tooltip"> ' + this.value + '</p>';
            }
        },
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: '<p></p>'
        }
    }, credits: {
      enabled: false
  },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table >',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} </td>' + '<td class="text-center" style="padding:0"><b>{point.y:.4f}<br>Votos</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0,
            borderWidth: 0
        }
    },
    series: [{
        name: '<p></p>',
        data: [49, 71, 106, 129, 144, 176, 135, 148, 216, 194, 95, 45, 300],
        color: 'rgb(99, 42, 91)',
        showInLegend: false
    }]
});