$(document).ready(function(){


 $('#mapa').vectorMap({
	   map: 'municipios',
                            backgroundColor: '#fff', // '#b3d1ff',
                            zoomButtons : false,
                            zoomOnScroll : false,
						    onRegionClick: function (event, poligono) {
						        console.log(poligono);
						    },regionStyle: {
                                initial: {
                                    fill: "#FFF",//"#f4f3f0",
                                    stroke: "#000000",
                                    "stroke-width": 4,
                                    "stroke-opacity": 0.5
                                },
                                selected: {
					                fill: '#c3207e'
					            },
                                hover: {
                                    fill: "#c3207e",
                                    "fill-opacity": "1"

                                }

                            }

	});

  //Se ejecuta al haber un cambio en el select de los partidos
    $("#selectPartido").change(function () {
            var partido =  $("#selectPartido").val();
            var map = $('#mapa').vectorMap('get', 'mapObject');
                var municipios = [1,2,3,4];
                var municipios2 = [5,6,7,8];
                
                //Se limpia el mapa de todos los poligonos
                map.clearSelectedRegions();

            if (partido == 2) {
                 for (var i = municipios.length - 1; i >= 0; i--) {
                    map.setSelectedRegions(municipios[i]); 
                } 
            }else if( partido == 3){
                 for (var i = municipios2.length - 1; i >= 0; i--) {
                    map.setSelectedRegions(municipios2[i]); 
                } 
            }
               
    });
    

});